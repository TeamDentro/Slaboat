package me.lignum.slaboat.lua;

import me.lignum.slaboat.Bot;
import me.lignum.slaboat.rtm.BotIdentity;
import me.lignum.slaboat.rtm.MessageData;
import me.lignum.slaboat.rtm.SlackSession;
import me.lignum.slaboat.util.FileUtil;
import org.json.JSONObject;
import org.luaj.vm2.Globals;
import org.luaj.vm2.LuaValue;
import org.luaj.vm2.lib.jse.JsePlatform;
import org.luaj.vm2.luajc.LuaJC;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

public class LuaBot extends Bot {
    private File botFolder;
    private File botConfigFile;
    private File botMainFile;

    private JSONObject botConfig;
    private BotIdentity identity;

    private LuaValue mainFunc;
    private Globals globals = JsePlatform.standardGlobals();

    public LuaBot(SlackSession session, String name) {
        super(session, name);

        botFolder = new File("bots" + File.separator + name);

        if (!botFolder.exists() || !botFolder.isDirectory()) {
            error("Failed to load bot '" + name + "': '" + botFolder.getPath() + "' is not a directory!\n");
            return;
        }

        botConfigFile = new File(botFolder, "bot.json");
        botConfig = new JSONObject(FileUtil.readAll(botConfigFile));

        if (!botConfig.has("bot-name")) {
            error("Missing 'bot-name' attribute in bot.json!!\n");
            return;
        }

        if (!(botConfig.has("avatar-url") ^ botConfig.has("avatar-emoji"))) {
            error("Invalid 'avatar-url' or 'avatar-emoji' attribute in bot.json!! Either duplicate or missing.\n");
            return;
        }

        identity = new BotIdentity(botConfig.getString("bot-name"));

        if (botConfig.has("avatar-url")) {
            identity.setAvatarFromURL(botConfig.getString("avatar-url"));
        } else {
            identity.setAvatarFromEmoji(botConfig.getString("avatar-emoji"));
        }

        botMainFile = new File(botFolder, "main.lua");

        setupLuaGlobals();

        LuaJC.install(globals);
        mainFunc = globals.load(FileUtil.readAll(botMainFile), name);
        globals.set("luajava", LuaValue.NIL);

        try (FileInputStream in = new FileInputStream(botMainFile)) {
            globals.loadPrototype(in, botMainFile.getName(), "bt");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        if (mainFunc != null) {
            mainFunc.call();
        } else {
            error("Failed to load bot " + name + "!!\n");
        }

        print("Loaded bot " + name + "\n");
    }

    private LuaBotAPI botAPI;
    private LuaSlackAPI slackAPI;

    private void setupLuaGlobals() {
        botAPI = new LuaBotAPI(this, globals);
        globals.set("Bot", botAPI);

        slackAPI = new LuaSlackAPI(getSession());
        globals.set("Slack", slackAPI);

        globals.set("HTTP", new LuaHTTP());
    }

    @Override
    public void onReceiveMessage(MessageData msg) {
        if (botAPI != null) {
            LuaValue func = botAPI.get("onMessage");

            if (func != null && func.isfunction()) {
                func.call(new LuaMessageData(msg));
            }
        }
    }

    @Override
    public void onDisconnected(String reason, boolean remote) {
        if (botAPI != null) {
            LuaValue func = botAPI.get("onDisconnected");

            if (func != null && func.isfunction()) {
                func.call(LuaValue.valueOf(reason), LuaValue.valueOf(remote));
            }
        }
    }

    @Override
    public BotIdentity getIdentity() {
        return identity;
    }

	@Override
	public void setIdentity(BotIdentity identity) {
		this.identity = identity;
	}
}
