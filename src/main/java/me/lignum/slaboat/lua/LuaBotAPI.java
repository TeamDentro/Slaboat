package me.lignum.slaboat.lua;

import me.lignum.slaboat.Bot;
import me.lignum.slaboat.rtm.Attachment;
import me.lignum.slaboat.rtm.BasicAttachment;
import me.lignum.slaboat.rtm.MessageBuilder;
import org.luaj.vm2.Globals;
import org.luaj.vm2.LuaTable;
import org.luaj.vm2.LuaValue;
import org.luaj.vm2.lib.OneArgFunction;
import org.luaj.vm2.lib.ThreeArgFunction;
import org.luaj.vm2.lib.ZeroArgFunction;

import java.util.ArrayList;
import java.util.List;

public class LuaBotAPI extends LuaTable {
    private final Bot bot;

    private interface IPrinter {
        void print(String s);
    }

    private class PrintFunction extends OneArgFunction {
        private IPrinter printer;

        public PrintFunction(IPrinter printer) {
            this.printer = printer;
        }

        @Override
        public LuaValue call(LuaValue arg) {
            printer.print(arg.checkjstring());
            return LuaValue.NIL;
        }
    }

    public LuaBotAPI(final Bot bot, Globals globals) {
        this.bot = bot;

        if (globals != null) {
            globals.set("print", new PrintFunction(new IPrinter() {
                @Override
                public void print(String s) {
                    bot.print(s);
                }
            }));

            globals.set("printError", new PrintFunction(new IPrinter() {
                @Override
                public void print(String s) {
                    bot.error(s);
                }
            }));
        }

        set("sendMessage", new ThreeArgFunction() {
            @Override
            public LuaValue call(LuaValue arg1, LuaValue arg2, LuaValue arg3) {
                LuaTable tbl = (LuaTable)arg1.checktable();

                if (tbl instanceof LuaChannel) {
                    LuaChannel channel = (LuaChannel) tbl;

                    List<Attachment> attachmentList = new ArrayList<Attachment>();

                    if (arg3.istable()) {
                        LuaTable attachs = (LuaTable)arg3;
                        for (LuaValue key : attachs.keys()) {
                            LuaValue val = attachs.get(key);

                            if (val.istable()) {
                                if (val instanceof LuaAttachment) {
                                    LuaAttachment at = (LuaAttachment) val;
                                    attachmentList.add(at.attachment);
                                }
                            }
                        }
                    }

                    bot.sendMessage(channel.channel, arg2.checkjstring(), attachmentList.toArray(new Attachment[0]));
                }

                return LuaValue.NIL;
            }
        });

        set("buildMessage", new ZeroArgFunction() {
            @Override
            public LuaValue call() {
                return new LuaMessage(new MessageBuilder(bot));
            }
        });

        set("buildAttachment", new ZeroArgFunction() {
            @Override
            public LuaValue call() {
                return new LuaAttachment(new BasicAttachment());
            }
        });
    }
}
