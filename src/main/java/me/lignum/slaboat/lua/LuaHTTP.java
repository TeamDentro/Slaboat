package me.lignum.slaboat.lua;

import me.lignum.slaboat.util.HTTPResponse;
import me.lignum.slaboat.util.HTTPUtil;
import org.luaj.vm2.LuaTable;
import org.luaj.vm2.LuaValue;
import org.luaj.vm2.Varargs;
import org.luaj.vm2.lib.OneArgFunction;
import org.luaj.vm2.lib.VarArgFunction;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class LuaHTTP extends LuaTable {
    public static final String[] HTTP_VERBS = {"GET", "POST", "PUT", "DELETE", "UPDATE", "HEAD", "OPTIONS", "PATCH"};

    public LuaHTTP() {
        for (final String verb : HTTP_VERBS) {
            set(verb.toLowerCase(), new VarArgFunction() {
                @Override
                public Varargs invoke(Varargs v) {
                    String url = v.arg(1).tojstring();
                    String body = v.arg(2).isnil() ? null : v.arg(2).tojstring();
                    Map<String, String> headers = new HashMap<>();

                    if (!v.arg(3).isnil()) {
                        LuaTable headersTable = (LuaTable) v.arg(3);

                        for (LuaValue header : headersTable.keys()) {
                            LuaValue value = headersTable.get(header);

                            headers.put(header.tojstring(), value.tojstring());
                        }
                    }

                    HTTPResponse response = HTTPUtil.request(verb, url, body, headers);

                    LuaTable responseHeaders = new LuaTable();

                    Map<String, List<String>> responseHeadersMap = response.getHeaders();

                    if (responseHeadersMap != null && responseHeadersMap.size() > 0) {
                        for (Map.Entry<String, List<String>> header : response.getHeaders().entrySet()) {
                            if (header == null || header.getKey() == null || header.getValue() == null || header.getKey().equals("null"))
                                continue;

                            StringBuilder value = new StringBuilder();

                            for (String s : header.getValue()) {
                                value.append(s);
                                value.append(", ");
                            }

                            responseHeaders.set(
                                    LuaValue.valueOf(
                                            header.getKey()),
                                    LuaValue.valueOf(
                                            value.toString().substring(0, value.toString().length() - ", ".length())));
                        }
                    }

                    return LuaValue.varargsOf(new LuaValue[] {
                            LuaValue.valueOf(response.getContent()),
                            LuaValue.valueOf(response.getStatusCode()),
                            responseHeaders
                    });
                }
            });
        }

        set("decodeSlackURL", new OneArgFunction() {
            @Override
            public LuaValue call(LuaValue arg) {
                return LuaValue.valueOf(arg.checkjstring().replaceAll("^<(.+)>$", "$1"));
            }
        });
    }
}
