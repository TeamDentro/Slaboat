package me.lignum.slaboat.lua;

import me.lignum.slaboat.rtm.Attachment;
import org.luaj.vm2.LuaTable;
import org.luaj.vm2.LuaValue;
import org.luaj.vm2.Varargs;
import org.luaj.vm2.lib.OneArgFunction;
import org.luaj.vm2.lib.ThreeArgFunction;
import org.luaj.vm2.lib.TwoArgFunction;
import org.luaj.vm2.lib.VarArgFunction;

import java.awt.*;

public class LuaAttachment extends LuaTable {
    public Attachment attachment;

    public LuaAttachment(final Attachment attach) {
        this.attachment = attach;

        set("text", new OneArgFunction() {
            @Override
            public LuaValue call(LuaValue arg) {
                attachment.text(arg.checkjstring());
                return LuaAttachment.this;
            }
        });

        set("pretext", new OneArgFunction() {
            @Override
            public LuaValue call(LuaValue arg) {
                attachment.pretext(arg.checkjstring());
                return LuaAttachment.this;
            }
        });

		set("thumbnail", new OneArgFunction() {
			@Override
			public LuaValue call(LuaValue arg) {
				attachment.thumbnail(arg.checkjstring());
				return LuaAttachment.this;
			}
		});

		set("image", new OneArgFunction() {
			@Override
			public LuaValue call(LuaValue arg) {
				attachment.image(arg.checkjstring());
				return LuaAttachment.this;
			}
		});

		set("title", new TwoArgFunction() {
            @Override
            public LuaValue call(LuaValue arg1, LuaValue arg2) {
                String titleURL = null;

                if (arg2.isstring()) {
                    titleURL = arg2.tojstring();
                }

                attachment.title(arg1.checkjstring(), titleURL);
                return LuaAttachment.this;
            }
        });

		set("author", new TwoArgFunction() {
			@Override
			public LuaValue call(LuaValue arg1, LuaValue arg2) {
				String authorURL = null;

				if (arg2.isstring()) {
					authorURL = arg2.tojstring();
				}

				attachment.author(arg1.checkjstring(), authorURL);
				return LuaAttachment.this;
			}
		});

		set("authorIcon", new OneArgFunction() {
			@Override
			public LuaValue call(LuaValue arg) {
				attachment.authorIcon(arg.checkjstring());
				return LuaAttachment.this;
			}
		});

        set("fallback", new OneArgFunction() {
            @Override
            public LuaValue call(LuaValue arg) {
                attachment.fallback(arg.checkjstring());
                return LuaAttachment.this;
            }
        });

        set("colour", new VarArgFunction() {
			@Override
			public Varargs invoke(Varargs args) {
				if (args.narg() == 1) {
					if (args.arg(1).isstring()) {
						attachment.colour(Color.decode(args.arg(1).checkjstring()));

					} else {
						error("invalid arguments");
					}
				} else if (args.narg() == 3) {
					LuaValue arg1 = args.arg(1);
					LuaValue arg2 = args.arg(2);
					LuaValue arg3 = args.arg(3);

					if (arg1.isint() && arg2.isint() && arg3.isint()) {
						int r = arg1.toint();
						int g = arg2.toint();
						int b = arg3.toint();

						Color col = new Color(r, g, b); // *Colour
						attachment.colour(col);
					} else if (arg1.isint() && arg2.isnil() && arg3.isnil()) {
						int rgb = arg1.toint();

						Color col = new Color(rgb);
						attachment.colour(col);
					} else {
						error("invalid arguments");
					}
				}

				return LuaAttachment.this;
			}

			@Override
            public LuaValue call(LuaValue arg1, LuaValue arg2, LuaValue arg3) {


                return LuaAttachment.this;
            }
        });
    }
}
