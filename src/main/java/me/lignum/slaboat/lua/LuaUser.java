package me.lignum.slaboat.lua;

import me.lignum.slaboat.rtm.User;
import org.luaj.vm2.LuaTable;
import org.luaj.vm2.LuaValue;
import org.luaj.vm2.lib.ZeroArgFunction;

public class LuaUser extends LuaTable {
    public User user;

    public LuaUser(final User user) {
        this.user = user;

        set("getName", new ZeroArgFunction() {
            @Override
            public LuaValue call() {
                return LuaValue.valueOf(user.getName());
            }
        });

        set("getFirstName", new ZeroArgFunction() {
            @Override
            public LuaValue call() {
                return LuaValue.valueOf(user.getFirstName());
            }
        });

        set("getLastName", new ZeroArgFunction() {
            @Override
            public LuaValue call() {
                return LuaValue.valueOf(user.getLastName());
            }
        });

        set("getFullName", new ZeroArgFunction() {
            @Override
            public LuaValue call() {
                return LuaValue.valueOf(user.getFullName());
            }
        });

        set("getEmail", new ZeroArgFunction() {
            @Override
            public LuaValue call() {
                return LuaValue.valueOf(user.getEmail());
            }
        });

        set("getPhoneNumber", new ZeroArgFunction() {
            @Override
            public LuaValue call() {
                return LuaValue.valueOf(user.getPhoneNumber());
            }
        });

        set("isAdmin", new ZeroArgFunction() {
            @Override
            public LuaValue call() {
                return LuaValue.valueOf(user.isAdmin());
            }
        });

        set("isOwner", new ZeroArgFunction() {
            @Override
            public LuaValue call() {
                return LuaValue.valueOf(user.isOwner());
            }
        });

        set("isDeleted", new ZeroArgFunction() {
            @Override
            public LuaValue call() {
                return LuaValue.valueOf(user.isDeleted());
            }
        });

        set("getUserID", new ZeroArgFunction() {
            @Override
            public LuaValue call() {
                return LuaValue.valueOf(user.getUserID());
            }
        });

        set("getSkypeName", new ZeroArgFunction() {
            @Override
            public LuaValue call() {
                return LuaValue.valueOf(user.getSkypeName());
            }
        });

        set("isSlaboatAdmin", new ZeroArgFunction() {
            @Override
            public LuaValue call() {
                return LuaValue.valueOf(user.isSlaboatAdmin());
            }
        });
    }
}
