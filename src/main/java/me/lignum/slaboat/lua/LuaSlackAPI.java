package me.lignum.slaboat.lua;

import me.lignum.slaboat.rtm.SlackSession;
import org.luaj.vm2.LuaTable;
import org.luaj.vm2.LuaValue;
import org.luaj.vm2.lib.OneArgFunction;

public class LuaSlackAPI extends LuaTable {
    public LuaSlackAPI(final SlackSession session) {
        set("getChannel", new OneArgFunction() {
            @Override
            public LuaValue call(LuaValue arg) {
                return new LuaChannel(session.getChannelByName(arg.checkjstring()));
            }
        });

        set("getUser", new OneArgFunction() {
            @Override
            public LuaValue call(LuaValue arg) {
                return new LuaUser(session.getUserByName(arg.checkjstring()));
            }
        });
    }
}
