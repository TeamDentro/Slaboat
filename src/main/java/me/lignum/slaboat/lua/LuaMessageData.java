package me.lignum.slaboat.lua;

import me.lignum.slaboat.rtm.MessageData;
import org.luaj.vm2.LuaTable;
import org.luaj.vm2.LuaValue;
import org.luaj.vm2.lib.ZeroArgFunction;

public class LuaMessageData extends LuaTable {
    public MessageData data;

    public LuaMessageData(final MessageData data) {
        this.data = data;

        set("getText", new ZeroArgFunction() {
            @Override
            public LuaValue call() {
                return LuaValue.valueOf(data.text);
            }
        });

        set("getChannel", new ZeroArgFunction() {
            @Override
            public LuaValue call() {
                return new LuaChannel(data.channel);
            }
        });

        set("getUser", new ZeroArgFunction() {
            @Override
            public LuaValue call() {
                //return LuaValue.valueOf(data.user);
                return LuaValue.NIL;
            }
        });
    }
}
