package me.lignum.slaboat.lua;

import me.lignum.slaboat.rtm.Attachment;
import me.lignum.slaboat.rtm.Channel;
import me.lignum.slaboat.rtm.MessageBuilder;
import org.luaj.vm2.LuaTable;
import org.luaj.vm2.LuaValue;
import org.luaj.vm2.Varargs;
import org.luaj.vm2.lib.OneArgFunction;
import org.luaj.vm2.lib.TwoArgFunction;
import org.luaj.vm2.lib.VarArgFunction;

import java.util.ArrayList;
import java.util.List;

public class LuaMessage extends LuaTable {
	public MessageBuilder builder;

	public LuaMessage(final MessageBuilder builder) {
		this.builder = builder;

		set("text", new OneArgFunction() {
			@Override
			public LuaValue call(LuaValue arg) {
				builder.text(arg.tojstring());

				return LuaMessage.this;
			}
		});

		set("username", new OneArgFunction() {
			@Override
			public LuaValue call(LuaValue arg) {
				builder.username(arg.tojstring());

				return LuaMessage.this;
			}
		});

		set("avatarURL", new OneArgFunction() {
			@Override
			public LuaValue call(LuaValue arg) {
				builder.avatarURL(arg.tojstring());

				return LuaMessage.this;
			}
		});

		set("avatarEmoji", new OneArgFunction() {
			@Override
			public LuaValue call(LuaValue arg) {
				builder.avatarEmoji(arg.tojstring());

				return LuaMessage.this;
			}
		});

		set("avatar", new OneArgFunction() {
			@Override
			public LuaValue call(LuaValue arg) {
				builder.avatar(arg.tojstring());

				return LuaMessage.this;
			}
		});

		set("attach", new VarArgFunction() {
			@Override
			public Varargs invoke(Varargs v) {
				List<Attachment> attachments = new ArrayList<>();

				if (v instanceof LuaAttachment) {
					attachments.add(((LuaAttachment) v).attachment);
				} else if (v instanceof LuaTable) {
					for (int i = 0; i < v.narg(); i++) {
						LuaValue attachment = v.arg(i);

						if (attachment instanceof LuaAttachment) {
							attachments.add(((LuaAttachment) attachment).attachment);
						}
					}
				}


				builder.attach(attachments.toArray(new Attachment[attachments.size()]));

				return LuaMessage.this;
			}
		});

		set("attachImage", new TwoArgFunction() {
			@Override
			public LuaValue call(LuaValue imageURL, LuaValue fallback) {
				if (!fallback.isnil()) {
					builder.attachImage(imageURL.tojstring(), fallback.tojstring());
				} else {
					builder.attachImage(imageURL.tojstring());
				}

				return LuaMessage.this;
			}
		});

		set("send", new VarArgFunction() {
			@Override
			public Varargs invoke(Varargs v) {
				List<Channel> channels = new ArrayList<>();

				if (v instanceof LuaChannel) {
					channels.add(((LuaChannel) v).channel);
				} else if (v instanceof LuaTable) {
					for (int i = 0; i < v.narg(); i++) {
						LuaValue channel = v.arg(i);

						if (channel instanceof LuaChannel) {
							channels.add(((LuaChannel) channel).channel);
						}
					}
				}

				builder.send(channels.toArray(new Channel[channels.size()]));

				return LuaMessage.this;
			}
		});
	}
}
