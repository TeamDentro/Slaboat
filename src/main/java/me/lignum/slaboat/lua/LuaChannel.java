package me.lignum.slaboat.lua;

import me.lignum.slaboat.rtm.Channel;
import org.luaj.vm2.LuaTable;
import org.luaj.vm2.LuaValue;
import org.luaj.vm2.lib.ZeroArgFunction;

import java.util.List;

public class LuaChannel extends LuaTable {
    public Channel channel;

    public LuaChannel(final Channel channel) {
        this.channel = channel;

        set("getChannelID", new ZeroArgFunction() {
            @Override
            public LuaValue call() {
                return LuaValue.valueOf(channel.getChannelID());
            }
        });

        set("getName", new ZeroArgFunction() {
            @Override
            public LuaValue call() {
                return LuaValue.valueOf(channel.getName());
            }
        });

        set("getPurpose", new ZeroArgFunction() {
            @Override
            public LuaValue call() {
                return LuaValue.valueOf(channel.getPurpose());
            }
        });

        set("getTopic", new ZeroArgFunction() {
            @Override
            public LuaValue call() {
                return LuaValue.valueOf(channel.getTopic());
            }
        });

        set("getMembers", new ZeroArgFunction() {
            @Override
            public LuaValue call() {
                LuaTable tbl = new LuaTable();
                List<String> members = channel.getMembers();

                for (int i = 0; i < members.size(); ++i) {
                    String member = members.get(i);
                    tbl.set(i + 1, member);
                }

                return tbl;
            }
        });
    }
}
