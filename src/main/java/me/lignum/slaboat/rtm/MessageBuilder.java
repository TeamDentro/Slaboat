package me.lignum.slaboat.rtm;

import me.lignum.slaboat.Bot;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class MessageBuilder {
	private Bot bot;
	private String text;
	private BotIdentity oldIdentity;
	private BotIdentity temporaryIdentity;
	private List<Attachment> attachments;

	public MessageBuilder(Bot bot) {
		this.bot = bot;
		this.text = "";
		this.oldIdentity = bot.getIdentity().copy();
		this.temporaryIdentity = bot.getIdentity().copy();
		this.attachments = new ArrayList<>();
	}

	public MessageBuilder text(String text) {
		this.text = text;

		return this;
	}

	public MessageBuilder username(String username) {
		this.temporaryIdentity.setName(username);

		return this;
	}

	public MessageBuilder avatarURL(String url) {
		this.temporaryIdentity.setAvatarFromURL(url);

		return this;
	}

	public MessageBuilder avatarEmoji(String emoji) {
		this.temporaryIdentity.setAvatarFromEmoji(emoji);

		return this;
	}

	public MessageBuilder avatar(String avatar) {
		if (avatar.startsWith(":") && avatar.endsWith(":")) {
			this.temporaryIdentity.setAvatarFromEmoji(avatar);
		} else {
			this.temporaryIdentity.setAvatarFromURL(avatar);
		}

		return this;
	}

	public MessageBuilder attach(Attachment... attachments) {
		this.attachments.addAll(Arrays.asList(attachments));

		return this;
	}

	public MessageBuilder attachImage(String imageURL) {
		this.attachments.add(new ImageAttachment(imageURL));

		return this;
	}

	public MessageBuilder attachImage(String imageURL, String fallback) {
		this.attachments.add(new ImageAttachment(imageURL, fallback));

		return this;
	}


	public MessageBuilder send(Channel... channels) {
		bot.setIdentity(temporaryIdentity);

		try {
			for (Channel channel : channels) {
				bot.sendMessage(channel, this.text, this.attachments.toArray(new Attachment[attachments.size()]));
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			bot.setIdentity(oldIdentity);
		}

		return this;
	}
}
