package me.lignum.slaboat.rtm;

public interface ISlackListener {
    void onMessage(MessageData data);
    void onConnected();
    void onDisconnected(String reason, boolean remotely);
}
