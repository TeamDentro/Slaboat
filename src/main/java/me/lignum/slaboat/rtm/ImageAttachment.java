package me.lignum.slaboat.rtm;

import org.json.JSONObject;

public class ImageAttachment extends Attachment {
    public ImageAttachment(String imageURL) {
        this.imageURL = imageURL;
        this.fallback = imageURL;
    }

    public ImageAttachment(String imageURL, String fallback) {
        this.imageURL = imageURL;
        this.fallback = fallback;
    }

    @Override
    public JSONObject asJSONObject() {
        JSONObject a = createTemplateObject();
        return a;
    }
}
