package me.lignum.slaboat.rtm;

import me.lignum.slaboat.util.GETParams;
import me.lignum.slaboat.util.HTTPResponse;
import me.lignum.slaboat.util.HTTPUtil;
import org.java_websocket.client.WebSocketClient;
import org.java_websocket.drafts.Draft_17;
import org.java_websocket.handshake.ServerHandshake;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import javax.net.ssl.SSLSocketFactory;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SlackSession {
	public static final String SLACK_API = "https://slack.com/api/";

	private WebSocketClient socket;
	private String token;

	private Map<String, Channel> channelCache = new HashMap<String, Channel>();
	private Map<String, User> userCache = new HashMap<String, User>();

	private List<User> ignoredUsers = new ArrayList<User>();

	public SlackSession(String token) {
		this.token = token;
	}

	public void ignoreUser(User user) {
		ignoredUsers.add(user);
	}

	public Channel getChannel(String id) {
		String lid = id.toUpperCase();

		if (channelCache.containsKey(lid)) {
			return channelCache.get(lid);
		} else {
			Channel ch = new Channel(this, lid);
			channelCache.put(lid, ch);
			return ch;
		}
	}

	private Map<String, String> idCache = new HashMap<String, String>();

	public void deleteCache() {
		idCache.clear();
	}

	private String getChannelID(String name) {
		if (idCache.containsKey(name.toLowerCase())) {
			return idCache.get(name.toLowerCase());
		}

		JSONObject channelList = performAPICall("channels.list", true, null);
		JSONArray channels = channelList.getJSONArray("channels");

		for (int i = 0; i < channels.length(); ++i) {
			JSONObject channel = channels.getJSONObject(i);

			if (channel.getString("name").equalsIgnoreCase(name)) {
				String id = channel.getString("id");
				idCache.put(name.toLowerCase(), id);
				return id;
			}
		}

		return null;
	}

	private String getUserID(String name) {
		if (idCache.containsKey(name.toLowerCase())) {
			return idCache.get(name.toLowerCase());
		}

		JSONObject userList = performAPICall("users.list", true, null);
		JSONArray users = userList.getJSONArray("members");

		for (int i = 0; i < users.length(); ++i) {
			JSONObject user = users.getJSONObject(i);

			if (user.getString("name").equalsIgnoreCase(name)) {
				String id = user.getString("id");
				idCache.put(name.toLowerCase(), id);
				return id;
			}
		}

		return null;
	}

	public User getUserByName(String name) {
		return getUser(getUserID(name));
	}

	public User getUser(String id) {
		if (id == null) {
			return null;
		}

		String lid = id.toUpperCase();

		if (userCache.containsKey(lid)) {
			return userCache.get(lid);
		} else {
			User ch = new User(this, lid);
			userCache.put(lid, ch);
			return ch;
		}
	}

	public Channel getChannelByName(String name) {
		String id = getChannelID(name);
		return getChannel(id);
	}

	public void start() {
		JSONObject response = performAPICall("rtm.start", true, null);
		String websockURL = null;

		if (!checkResponse(response)) {
			return;
		}

		try {
			websockURL = response.getString("url");
		} catch (JSONException e) {
			e.printStackTrace();
		}

		System.out.println("Got websocket URL: " + websockURL);

		if (socket != null) {
			try {
				socket.closeBlocking();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}

		URI sockURI = null;
		try {
			 sockURI = new URI(websockURL);
		} catch (URISyntaxException e) {
			e.printStackTrace();
		}

		createWebsocket(sockURI);
	}

	public void postMessage(BotIdentity id, Channel channel, String message, Attachment... attachments) {
		GETParams params = new GETParams();
		params.addParam("username", id.name);
		params.addParam("text", message);
		params.addParam("as_user", "false");
		params.addParam("channel", channel.getChannelID());
		params.addParam("parse", "full");
		params.addParam("unfurl_media", "true");

		if (id.avatarIsEmoji) {
			params.addParam("icon_emoji", id.avatarURL);
		} else {
			params.addParam("icon_url", id.avatarURL);
		}

		if (attachments != null && attachments.length > 0) {
			JSONArray arr = new JSONArray();

			for (Attachment a : attachments) {
				arr.put(a.asJSONObject());
			}

			params.addParam("attachments", arr.toString());
		}

		performAPICall("chat.postMessage", true, params);
	}

	private List<ISlackListener> subscribed = new ArrayList<ISlackListener>();

	public void addSlackListener(ISlackListener listener) {
		subscribed.add(listener);
	}

	public void removeSlackListener(ISlackListener listener) {
		subscribed.remove(listener);
	}

	public boolean isIgnored(User user) {
		return ignoredUsers.contains(user);
	}

	private void createWebsocket(URI uri) {
		SSLSocketFactory factory = (SSLSocketFactory) SSLSocketFactory.getDefault();

		socket = new WebSocketClient(uri, new Draft_17()) {
			@Override
			public void onOpen(ServerHandshake handshakedata) {
				System.out.println("Connected to Slack!");
			}

			@Override
			public void onMessage(String message) {
				JSONObject msg = new JSONObject(message);

				if (msg.has("type")) {
					String type = msg.getString("type");

					if (type.equalsIgnoreCase("message")) {
						if (msg.has("user") && msg.has("channel") && msg.has("text")){
							MessageData data = new MessageData();
							data.channel = getChannel(msg.getString("channel"));
							data.user = getUser(msg.getString("user"));
							data.text = msg.getString("text");

							if (!isIgnored(data.user)) {
								for (ISlackListener listener : subscribed) {
									try {
										listener.onMessage(data);
									} catch (Exception e) {
										e.printStackTrace();
									}
								}
							}
						}
					} else if (type.equalsIgnoreCase("hello")) {
						System.out.println("Connection fully established!");

						for (ISlackListener listener : subscribed) {
							listener.onConnected();
						}
					}
				}
			}

			@Override
			public void onClose(int code, String reason, boolean remote) {
				for (ISlackListener listener : subscribed) {
					listener.onDisconnected(reason, remote);
				}

				System.err.println("Connection closed" + (remote ? " by remote" : "") + ":" + reason);
			}

			@Override
			public void onError(Exception ex) {
				System.err.println("Websocket error: ");
				ex.printStackTrace();
			}
		};

		try {
			socket.setSocket(factory.createSocket());

			if (!socket.connectBlocking()) {
				System.err.println("Failed to connect!!");
            }
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private static boolean checkResponse(JSONObject resp) {
		boolean isOK = resp.getBoolean("ok");
		if (!isOK) {
			System.err.println("Slack API returned error: " + resp.getString("error"));
		}

		return isOK;
	}
	
	public JSONObject performAPICall(String method, boolean withToken, GETParams additionalParams) {
		GETParams params = new GETParams();
		
		if (withToken) {
			params.addParam("token", token);
		}
		
		if (additionalParams != null) {
			params = params.combine(additionalParams);
		}
		
		String requestURL = SLACK_API + method + params.toString();
		HTTPResponse response = HTTPUtil.request("get", requestURL, null, null);

		JSONObject obj = new JSONObject(response.getContent());
		checkResponse(obj);
		return obj;
	}
}
