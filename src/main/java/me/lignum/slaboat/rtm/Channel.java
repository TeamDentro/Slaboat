package me.lignum.slaboat.rtm;

import me.lignum.slaboat.util.GETParams;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Channel {
    private String channelID;
    private String channelName;
    private String creator;
    private String channelTopic;
    private String channelPurpose;

    private List<String> members;

    private SlackSession session;

    protected Channel(SlackSession session, String channelID) {
        this.channelID = channelID;
        this.session = session;
    }

    private void downloadChannelInfo() {
        GETParams params = new GETParams();
        params.addParam("channel", channelID);
        JSONObject channelInfo = session.performAPICall("channels.info", true, params);
        channelInfo = channelInfo.getJSONObject("channel");

        channelName = channelInfo.getString("name");
        creator = channelInfo.getString("creator");

        members = new ArrayList<String>();
        JSONArray mems = channelInfo.getJSONArray("members");

        for (int i = 0; i < mems.length(); ++i) {
            String member = mems.getString(i);
            members.add(member);
        }

        channelTopic = channelInfo.getJSONObject("topic").getString("value");
        channelPurpose = channelInfo.getJSONObject("purpose").getString("value");
    }

    public void deleteCache() {
        downloadChannelInfo();
    }

    public List<String> getMembers() {
        if (members == null) {
            downloadChannelInfo();
        }

        return Collections.unmodifiableList(members);
    }

    public String getChannelID() {
        return channelID;
    }

    public String getName() {
        if (channelName == null) {
            downloadChannelInfo();
        }

        return channelName;
    }

    public String getTopic() {
        if (channelTopic == null) {
            downloadChannelInfo();
        }

        return channelTopic;
    }

    public String getPurpose() {
        if (channelPurpose == null) {
            downloadChannelInfo();
        }

        return channelPurpose;
    }
}
