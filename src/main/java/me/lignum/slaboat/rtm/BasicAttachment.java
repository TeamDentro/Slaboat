package me.lignum.slaboat.rtm;

import org.json.JSONObject;

public class BasicAttachment extends Attachment {
	@Override
	public JSONObject asJSONObject() {
		JSONObject a = createTemplateObject();
		return a;
	}
}
