package me.lignum.slaboat.rtm;

public class BotIdentity {
    public String name;
    public String avatarURL;
    public boolean avatarIsEmoji = false;

    public BotIdentity(String name) {
        this.name = name;
    }

    public void setAvatarFromURL(String url) {
        this.avatarURL = url;
        this.avatarIsEmoji = false;
    }

    public void setAvatarFromEmoji(String emoji) {
        this.avatarURL = emoji;
        this.avatarIsEmoji = true;
    }

    public void setName(String name) {
        this.name = name;
    }

    public BotIdentity copy() {
        BotIdentity newIdentity = new BotIdentity(name);

        newIdentity.avatarURL = avatarURL;
        newIdentity.avatarIsEmoji = avatarIsEmoji;

        return newIdentity;
    }
}
