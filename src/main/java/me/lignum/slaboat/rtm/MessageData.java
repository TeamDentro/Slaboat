package me.lignum.slaboat.rtm;

public class MessageData {
    public Channel channel;
    public String text;
    public User user;
}
