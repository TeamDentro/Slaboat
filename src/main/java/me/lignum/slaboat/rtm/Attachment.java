package me.lignum.slaboat.rtm;

import org.json.JSONObject;

import java.awt.*;

public abstract class Attachment {
    protected String pretext = null;
    protected String text = null;
    protected String fallback = null;
    protected Color colour = null; // *Colour
    protected String title = null, titleLink = null;
    protected String author = null, authorLink = null, authorIcon = null;
    protected String thumbURL = null;
    protected String imageURL = null;

    public Attachment text(String text) {
        this.text = text;
        return this;
    }

    public Attachment pretext(String preText) {
        this.pretext = preText;
        return this;
    }

    public Attachment colour(Color colour) { // *Colour
        this.colour = colour;
        return this;
    }

    public Attachment title(String title, String titleLink) {
        this.title = title;
        this.titleLink = titleLink;
        return this;
    }

    public Attachment title(String title) {
        this.title = title;
        this.titleLink = null;
        return this;
    }

    public Attachment author(String name, String link) {
        this.author = name;
        this.authorLink = link;
        return this;
    }

    public Attachment author(String name) {
        this.author = name;
        this.authorLink = null;
        return this;
    }

    public Attachment authorIcon(String icon) {
        this.authorIcon = icon;
        return this;
    }

    public Attachment thumbnail(String url) {
        this.thumbURL = url;
        return this;
    }

    public Attachment image(String url) {
        this.imageURL = url;
        return this;
    }

    public Attachment fallback(String fallback) {
        this.fallback = fallback;
        return this;
    }

    protected JSONObject createTemplateObject() {
        JSONObject obj = new JSONObject();

        if (fallback != null) {
            obj.put("fallback", fallback);
        }

        if (colour != null) {
            String colourString = Integer.toHexString(colour.getRGB());
            colourString = colourString.substring(2, colourString.length()); // Colour.getRGB actually returns ARGB... Thanks Obama.
            obj.put("color", "#" + colourString); // *colour
        }

        if (title != null) {
            obj.put("title", title);

            if (titleLink != null) {
                obj.put("title_link", titleLink);
            }
        }

        if (author != null) {
            obj.put("author_name", author);

            if (authorLink != null) {
                obj.put("author_link", authorLink);
            }

            if (authorIcon != null) {
                obj.put("author_icon", authorIcon);
            }
        }

        if (pretext != null) {
            obj.put("pretext", pretext);
        }

        if (text != null) {
            obj.put("text", text);
        }

        if (thumbURL != null) {
            obj.put("thumb_url", thumbURL);
        }

        if (imageURL != null) {
            obj.put("image_url", imageURL);
        }

        return obj;
    }

    public abstract JSONObject asJSONObject();
}
