package me.lignum.slaboat.rtm;

import me.lignum.slaboat.Slaboat;
import me.lignum.slaboat.util.GETParams;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class User {
    private String userID;
    private String userName;
    private String firstName, lastName;
    private String email;
    private String phoneNumber;
    private String skypeName;

    private SlackSession session;

    private Map<String, Boolean> flags = new HashMap<String, Boolean>();

    public User(SlackSession session, String id) {
        this.session = session;
        this.userID = id;
    }

    public String getUserID() {
        return userID;
    }

    private void downloadUserInfo() {
        GETParams params = new GETParams();
        params.addParam("user", userID);

        JSONObject info = session.performAPICall("users.info", true, params);
        info = info.getJSONObject("user");

        userName = info.getString("name");

        JSONObject profile = info.getJSONObject("profile");
        firstName = profile.getString("first_name");
        lastName = profile.getString("last_name");
        email = profile.getString("email");
        phoneNumber = profile.getString("phone");
        skypeName = profile.getString("skype");

        flags.put("admin", info.getBoolean("is_admin"));
        flags.put("owner", info.getBoolean("is_owner"));
        flags.put("deleted", info.getBoolean("deleted"));
    }

    public String getName() {
        if (userName == null) {
            downloadUserInfo();
        }

        return userName;
    }

    public String getFirstName() {
        if (firstName == null) {
            downloadUserInfo();
        }

        return firstName;
    }

    public String getLastName() {
        if (lastName == null) {
            downloadUserInfo();
        }

        return lastName;
    }

    public String getFullName() {
        return getFirstName() + " " + getLastName();
    }

    public String getEmail() {
        if (email == null) {
            downloadUserInfo();
        }

        return email;
    }

    public String getPhoneNumber() {
        if (phoneNumber == null) {
            downloadUserInfo();
        }

        return phoneNumber;
    }

    public String getSkypeName() {
        if (skypeName == null) {
            downloadUserInfo();
        }

        return skypeName;
    }

    public boolean isAdmin() {
        if (!flags.containsKey("admin")) {
            downloadUserInfo();
        }

        return flags.get("admin");
    }

    public boolean isOwner() {
        if (!flags.containsKey("owner")) {
            downloadUserInfo();
        }

        return flags.get("owner");
    }

    public boolean isDeleted() {
        if (!flags.containsKey("deleted")) {
            downloadUserInfo();
        }

        return flags.get("deleted");
    }

    public boolean isSlaboatAdmin() {
        return Slaboat.instance.isSlaboatAdmin(getName());
    }
}
