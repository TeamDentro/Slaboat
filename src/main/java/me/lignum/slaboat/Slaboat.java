package me.lignum.slaboat;

import me.lignum.slaboat.lua.LuaBot;
import me.lignum.slaboat.rtm.ISlackListener;
import me.lignum.slaboat.rtm.MessageData;
import me.lignum.slaboat.rtm.SlackSession;
import me.lignum.slaboat.rtm.User;
import org.apache.commons.lang3.RandomStringUtils;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.Random;

public class Slaboat implements Runnable, ISlackListener {
    public static final File CONFIG_FILE = new File("config.properties");
    private Properties props = new Properties();

    private List<Bot> bots = new ArrayList<Bot>();
    private List<Bot> botQueue = new ArrayList<Bot>();

    public static final String VERSION = "1.0";
    public static Slaboat instance;

    public Properties getProperties() {
        return props;
    }

    public boolean isSlaboatAdmin(String name) {
        String adminList = props.getProperty("admins");
        if (adminList != null) {
            String[] admins = adminList.split(",");

            for (String admin : admins) {
                if (admin.equalsIgnoreCase(name)) {
                    return true;
                }
            }
        }

        return false;
    }

    private void readConfig() throws IOException {
        if (!CONFIG_FILE.exists()) {
            CONFIG_FILE.createNewFile();
        }

        props.load(new FileReader(CONFIG_FILE));

        if (props.isEmpty()) {
            // set default properties
            props.setProperty("slack-api-token", "");
            props.setProperty("bots", "");
            props.setProperty("ignored-users", "");
            props.setProperty("admins", "");
            props.setProperty("slaboat-command", "!slaboat-" + RandomStringUtils.randomAlphanumeric(4));

            props.store(new FileWriter(CONFIG_FILE), "This is the main Slaboat configuration");
        }

		if (props.getProperty("slaboat-command") == null) {
			props.setProperty("slaboat-command", "!slaboat-" + RandomStringUtils.randomAlphanumeric(4));

			props.store(new FileWriter(CONFIG_FILE), "This is the main Slaboat configuration");
		}

        System.out.println("Your Slaboat command is " + props.getProperty("slaboat-command"));
    }

    private SlackSession api;

    @Override
    public void run() {
        try {
            readConfig();
        } catch (IOException e) {
            e.printStackTrace();
        }

        String token = props.getProperty("slack-api-token");
        if (!token.matches("xox(?:(?:s|p)-\\d{11}-\\d{11}-\\d{11}-(?:[a-f0-9]{10})|b-\\d{11}-(?:[A-Za-z0-9]{24}))")) {
            System.err.println("Invalid Slack API token!!");
        }

        api = new SlackSession(token);
        api.addSlackListener(this);

        api.start();
    }

    public boolean disableBot(String botID, String reason) {
        int toRemove = -1;

        for (int i = 0; i < bots.size(); ++i) {
            Bot b = bots.get(i);
            if (b.getBotID().equalsIgnoreCase(botID)) {
                b.onDisconnected(reason, false);
                toRemove = i;
                break;
            }
        }

        if (toRemove >= 0) {
            bots.remove(toRemove);
            return true;
        } else {
            return false;
        }
    }

    public boolean enableBot(String botID) {
        File botFile = new File("bots/" + botID);
        if (!botFile.isDirectory()) {
            return false;
        }

        LuaBot bot = new LuaBot(api, botID);
        return addBot(bot, true);
    }

    public boolean addBot(Bot bot, boolean reload) {
        for (Bot b : bots) {
            if (b.getBotID().equalsIgnoreCase(bot.getBotID())) {
                return false;
            }
        }

        bots.add(bot);
        return true;
    }

    private void loadBots() {
        bots.add(new SlaboatBot(api, "slaboat-bot"));

        String botString = props.getProperty("bots");

        if (botString != null) {
            String[] botList = botString.split(",");

            for (String botID : botList) {
                LuaBot bot = new LuaBot(api, botID);
                addBot(bot, false);
            }
        }

        String userIgnoreList = props.getProperty("ignored-users");

        if (userIgnoreList != null && !userIgnoreList.isEmpty()) {
            String[] ignoreList = userIgnoreList.split(",");

            for (String user : ignoreList) {
                User u = null;

                if (user.matches("/^u[a-z0-9]{8}$/i")) { // it's a UID
                    u = api.getUser(user);
                } else {
                    u = api.getUserByName(user);
                }

                if (u == null) {
                    System.err.println("Can't ignore unknown user \"" + user + "\"!!");
                } else {
                    api.ignoreUser(u);
                }
            }
        }
    }

    @Override
    public void onMessage(MessageData data) {
        for (int i = 0; i < bots.size(); ++i) {
            Bot bot = bots.get(i);
            bot.onReceiveMessage(data);
        }
    }

    @Override
    public void onConnected() {
        loadBots();
    }

    @Override
    public void onDisconnected(String reason, boolean remotely) {
        for (Bot bot : bots) {
            bot.onDisconnected(reason, remotely);
        }
    }
}
