package me.lignum.slaboat;

import me.lignum.slaboat.rtm.*;

import java.io.IOException;
import java.io.OutputStream;

public abstract class Bot {
    private OutputStream STDOUT = System.out;
    private OutputStream STDERR = System.err;

    private SlackSession session;
    private String botID;

    public Bot(SlackSession session, String botID) {
        this.session = session;
        this.botID = botID;
    }

    public SlackSession getSession() {
        return session;
    }

    public void setOutputTarget(OutputStream out) {
        STDOUT = out;
    }

    public void setErrorTarget(OutputStream out) {
        STDERR = out;
    }

    public void print(String msg) {
        try {
            STDOUT.write(msg.getBytes());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void error(String msg) {
        try {
            STDERR.write(msg.getBytes());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public String getBotID() {
        return botID;
    }

    public void onReceiveMessage(MessageData msg) {

    }

    public void onDisconnected(String reason, boolean remote) {

    }

    public abstract BotIdentity getIdentity();
    public abstract void setIdentity(BotIdentity identity);

    public void sendMessage(Channel channel, String message, Attachment... attachments) {
        BotIdentity identity = getIdentity();

        if (identity == null) {
            error("Can't send message because the bot has no identity!!\n");
            return;
        }

        session.postMessage(identity, channel, message, attachments);
    }
}
