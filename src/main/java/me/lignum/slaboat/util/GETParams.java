package me.lignum.slaboat.util;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

public class GETParams {
	public static class GETParam {
		public String key, value;
		
		public GETParam(String key, String value) {
			this.key = key;
			this.value = value;
		}
	}
	
	private List<GETParam> params = new ArrayList<GETParam>();
	
	public void addParam(GETParam param) {
		if (getValue(param.key) != null) {
			return;
		}
		
		params.add(param);
	}
	
	public void addParam(String key, String value) {
		params.add(new GETParam(key, value));
	}
	
	public String getValue(String key) {
		for (GETParam p : params) {
			if (p.key.equalsIgnoreCase(key)) {
				return p.value;
			}
		}
		
		return null;
	}
	
	public void removeParam(String key) {
		int toRemove = -1;
		
		for (int i = 0; i < params.size(); ++i) {
			GETParam p = params.get(i);
			
			if (p.key.equalsIgnoreCase(key)) {
				toRemove = i;
			}
		}
		
		if (toRemove >= 0) {
			params.remove(toRemove);
		}
	}
	
	public GETParams combine(GETParams params) {
		GETParams finalParams = new GETParams();
		
		for (GETParam p : params.params) {
			finalParams.addParam(p);
		}
		
		for (GETParam p : this.params) {
			finalParams.addParam(p);
		}
		
		return finalParams;
	}

	@Override
	public String toString() {
		if (params.size() == 0) {
			return "";
		}
		
		StringBuilder builder = new StringBuilder();
		builder.append("?");
		
		for (int i = 0; i < params.size(); ++i) {
			GETParam p = params.get(i);

			try {
				builder.append(URLEncoder.encode(p.key, "UTF-8"));
				builder.append("=");
				builder.append(URLEncoder.encode(p.value, "UTF-8"));
			} catch (UnsupportedEncodingException e) {
				e.printStackTrace();
			}

			if (i < params.size() - 1) {
				builder.append("&");
			}
		}

		return builder.toString();
	}
}
