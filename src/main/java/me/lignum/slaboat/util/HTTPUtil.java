package me.lignum.slaboat.util;

import javax.net.ssl.HttpsURLConnection;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;
import java.util.Map;

public class HTTPUtil {
	private static HttpURLConnection establishConnection(String method, String destination, Map<String, String> headers) {
		try {
			URL url = new URL(destination);

			HttpURLConnection con;

			if (url.getProtocol().equalsIgnoreCase("https")) {
				con = (HttpsURLConnection) url.openConnection();
			} else {
				con = (HttpURLConnection) url.openConnection();
			}

			con.setRequestMethod(method.toUpperCase());
			con.setRequestProperty("Accept-Charset", "UTF-8");
			con.setRequestProperty("Content-Type", "application/x-www-form-urlencoded; charset=utf-8");
			con.setRequestProperty("User-Agent", "Slaboat");

			if (headers != null) {
				for (Map.Entry<String, String> header : headers.entrySet()) {
					con.setRequestProperty(header.getKey(), header.getValue());
				}
			}

			return con;
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		return null;
	}

	public static HTTPResponse request(String method, String url, String body, Map<String, String> headers) {
		HttpURLConnection con = establishConnection(method, url, headers);
		BufferedReader in = null;
		DataOutputStream out = null;

		try {
			if (body != null && !body.isEmpty()) {
				con.setDoOutput(true);

				out = new DataOutputStream(con.getOutputStream());
				out.writeUTF(body);
				out.close();
			}

			int statusCode = con.getResponseCode();
			Map<String, List<String>> responseHeaders = con.getHeaderFields();

			try {
				in = new BufferedReader(new InputStreamReader(con.getInputStream()));
			} catch (IOException e) {
				in = new BufferedReader(new InputStreamReader(con.getErrorStream()));
			}

			String line;
			StringBuffer content = new StringBuffer();

			while ((line = in.readLine()) != null) {
				content.append(line);
			}

			in.close();

			return new HTTPResponse(content.toString(), statusCode, responseHeaders);
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				if (in != null) {
					in.close();
				}
				
				if (out != null) {
					out.close();
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		return null;
	}
}
