package me.lignum.slaboat.util;

import java.util.List;
import java.util.Map;

public class HTTPResponse {
	private String content;
	private int statusCode;
	private Map<String, List<String>> headers;

	public HTTPResponse(String content, int statusCode, Map<String, List<String>> headers) {
		this.content = content;
		this.statusCode = statusCode;
		this.headers = headers;
	}

	public String getContent() {
		return content;
	}

	public int getStatusCode() {
		return statusCode;
	}

	public Map<String, List<String>> getHeaders() {
		return headers;
	}
}