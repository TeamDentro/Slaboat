package me.lignum.slaboat;

import me.lignum.slaboat.rtm.*;

import java.util.Arrays;

public class SlaboatBot extends Bot {
    private BotIdentity identity;

    public SlaboatBot(SlackSession session, String botID) {
        super(session, botID);

        identity = new BotIdentity("Slaboat");
        identity.setAvatarFromEmoji(":robot_face:");
    }

    private void execute(User sender, Channel chan, String cmd, String[] args) {
        print("Slaboat Command from " + sender.getName() + ": " + Arrays.toString(args) + "\n");

        switch (cmd.toLowerCase()) {
            case "version":
                sendMessage(chan, "Running Slaboat Framework version " + Slaboat.VERSION + ".");
                break;

			case "reload":
            case "disable":
            case "enable":
                if (!sender.isSlaboatAdmin()) {
                    sendMessage(chan, "Error: You don't have permission to do this.");
                    return;
                }

                if (args.length > 2) {
                    String botName = args[2];
                    String reason = "";

                    for (int i = 3; i < args.length; ++i) {
                        reason += args[i] + " ";
                    }

                    if (reason.isEmpty()) {
                        reason = "Bot was disabled by admin.";
                    } else {
                        reason = reason.substring(0, reason.length() - 1);
                    }

                    switch (cmd) {
						case "enable":
							if (!Slaboat.instance.enableBot(botName)) {
								sendMessage(chan, "Error: Could not enable bot with ID '" + botName + "'!");
								return;
							} else {
								sendMessage(chan, "Loaded bot '" + botName + "'.");
								return;
							}
						case "disable":
							if (!Slaboat.instance.disableBot(botName, reason)) {
								sendMessage(chan, "Error: No bot with ID '" + botName + "'!");
								return;
							} else {
								sendMessage(chan, "Disabled bot '" + botName + "'.");
								return;
							}
						case "reload":
							if (!Slaboat.instance.disableBot(botName, reason)) {
								sendMessage(chan, "Error: No bot with ID '" + botName + "'!");
								return;
							}

							if (!Slaboat.instance.enableBot(botName)) {
								sendMessage(chan, "Error: Could not enable bot with ID '" + botName + "'!");
								return;
							} else {
								sendMessage(chan, "Reloaded bot '" + botName + "'.");
								return;
							}
					}
                } else {
                    sendMessage(chan, "Usage: !slaboat " + cmd.toLowerCase() + " <bot> [reason]");
                    return;
                }
            default:
                sendMessage(chan, "Running Slaboat Framework version " + Slaboat.VERSION + ".");
        }
    }

    @Override
    public void onReceiveMessage(MessageData msg) {
        Channel chan = msg.channel;
        String text = msg.text;

        if (text.startsWith("!")) {
            String cmdParts[] = text.split("\\s");
            if (cmdParts.length == 0) {
                return;
            }

            String cmd = cmdParts[0];

            if (cmd.equalsIgnoreCase(Slaboat.instance.getProperties().getProperty("slaboat-command", "!slaboat"))) {
                if (cmdParts.length > 1) {
                    String subcmd = cmdParts[1];
                    execute(msg.user, chan, subcmd, cmdParts);
                }
            }
        }
    }

    @Override
    public BotIdentity getIdentity() {
        return identity;
    }

	@Override
	public void setIdentity(BotIdentity identity) {
		this.identity = identity;
	}
}
