package me.lignum.slaboat;

public class Launch {
	public static void main(String[] args) {
		Slaboat slab = new Slaboat();
		Slaboat.instance = slab;

		System.out.println("Loading Slaboat...");

		slab.run();
	}
}
