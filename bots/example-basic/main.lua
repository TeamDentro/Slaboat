local commands = {
	test = function(chan)
		return Bot.buildMessage()
		.text("Test")
		.send(chan)
	end,

	test2 = function(chan)
		return Bot.buildMessage()
		.text("Test")
		.username("Testing temporary identities")
		.avatarURL("https://api.adorable.io/avatars/256/slaboat-test")
		.send(chan)
	end,

	test3 = function(chan)
		return Bot.buildMessage()
		.text("Test")
		.username("Image attachments")
		.attachImage("https://api.adorable.io/avatars/256/slaboat-test")
		.send(chan)
	end,

	test4 = function(chan)
		return Bot.buildMessage()
		.username("Textless images")
		.attachImage("https://api.adorable.io/avatars/256/slaboat-test")
		.send(chan)
	end,

	test5 = function(chan)
		local attachment = Bot.buildAttachment()
		.pretext("Attachment pretext")
		.title("Attachment", "https://lemmmy.pw")
		.author("Drew Lemmy", "https://lemmmy.pw")
		.authorIcon("https://lemmmy.pw/img/dentro_avi.png")
		.thumbnail("https://lemmmy.pw/img/header_photo_new_1280.jpg")
		.image("https://lemmmy.pw/img/header_photo_new_1280.jpg") -- overrides thumbnails
		.text("Attachment text")
		.fallback("Attachment fallback")
		.colour("#BB1177")

		return Bot.buildMessage()
		.username("Attachments")
		.text("Message text")
		.attach(attachment)
		.send(chan)
	end,
}

function Bot.onMessage(data)
	local text = data.getText()
	local chan = data.getChannel()

	local command, argStr = text:match("^%!(%w+)(.-)$")

	if command == nil then
		return
	end

	local args = {}
	if argStr ~= nil then
		argStr = argStr:gsub("^%s+", "")

		for arg in argStr:gmatch("[^%s]+") do
			args[#args + 1] = arg
		end
	end

	if commands[command] ~= nil then
		commands[command](chan, table.unpack(args))
	end
end

function Bot.onDisconnected(reason)
	return print("Bot was disconnected for reason: " .. tostring(reason) .. "\n")
end
